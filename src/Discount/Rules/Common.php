<?php

namespace Task\Discount\Rules;

use Task\IOrderProcessor;

class Common implements IRule
{
    private $count;
    private $exclude;
    private $discount;

    public function __construct($rule)
    {
        $this->count = $rule['count'];
        $this->exclude = $rule['exclude'];
        $this->discount = $rule['discount'];
    }

    public function setDiscount(IOrderProcessor $order)
    {
        $count_items = 0;

        foreach ($order->getList() as $key => $product) {
            $product_name = $product->getName();
            if (!in_array($product_name, $this->exclude, true)) {
                $count_items++;
            }
        }

        if ($count_items === $this->count) {
            $order->setCommonDiscount($this->discount);
        }
    }
}