<?php

namespace Task;

use Task\Product\IProduct;

interface IOrderProcessor
{
    public function getList();

    public function addItem(IProduct $item);

    public function deleteItem($id);

    public function setCommonDiscount($discount);

    public function getCommonDiscount();
}