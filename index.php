<?php

use Task\OrderProcessor;

require_once __DIR__ . '/vendor/autoload.php';

$order_list = ['A', 'B', 'E', 'F', 'G'];

$order = new OrderProcessor($order_list);

echo $order->getTotalSum();
